import random
import time
import dummy_logger
import threading
import connection
import gevent

##################################
#	GLOBALS 	         #
##################################

logger = dummy_logger

##################################
#			         #
##################################

def setLogger(logger_module):
    global logger
    logger = logger_module

class Connection(object):
    
    # Don't put the connection in the pool after this many seconds so that any load
    # balancer we have in place can still distribute connections (ie. if a connection
    # never expires or goes stale then a new server added will never be utilized)
    EXPIRE_TIME = 100000000
    
    def __init__(self, certificate, host_info):
        self.id = random.randint(0, 1000000)
        logger.log_debug("[%s] creating APNS connection to [%s:%s, %s]" % (id(gevent.getcurrent()), host_info[0], host_info[1], self.id))
        self.apns_connection = connection.APNSConnection(certificate = certificate, force_ssl_command = False, debug = False)
        self.apns_connection.connect(*host_info)
        self.endpoint = host_info
        self.timestamp = time.time()
    
    def expireConnection(self):
        now = time.time()
        return self.timestamp + Connection.EXPIRE_TIME < now
    
    def send(self, message):
        logger.log_debug('[%s] write - "%s" [%s:%s, %s]' % (id(gevent.getcurrent()), message, self.endpoint[0], self.endpoint[1], self.id))
        self.apns_connection.write(message)
    
    def close(self):
        logger.log_debug('[%s] closing connection to [%s:%s, %s]' % (id(gevent.getcurrent()), self.endpoint[0], self.endpoint[1], self.id))
        self.apns_connection.close()

class HostConnectionPool(object):
    def __init__(self):
        self.queue = []

    def size(self):
        """
        Returns the number of connections in the pool for this host.
        Some of the connections may still be in use, and may not be
        ready to be returned by get().
        """
        return len(self.queue)
    
    def put(self, conn):
        """
        Adds a connection to the pool, along with the time it was
        added.
        """
        self.queue.append((conn, time.time()))

    def get(self):
        """
        Returns the next connection in this pool that is ready to be
        reused.  Returns None of there aren't any.
        """
        # Discard ready connections that are too old.
        self.clean()

        # Return the first connection that is ready, and remove it
        # from the queue.  Connections that aren't ready are returned
        # to the end of the queue with an updated time, on the
        # assumption that somebody is actively reading the response.
        for _ in range(len(self.queue)):
            (conn, _) = self.queue.pop(0)
            if self._conn_ready(conn):
                logger.log_debug("[%s] reusing connection to [%s:%s, %s]" % (id(gevent.getcurrent()), conn.endpoint[0], conn.endpoint[1], conn.id))
                return conn
            else:
                self.put(conn)
        return None

    def _conn_ready(self, conn):
        return True

    def clean(self):
        """
        Get rid of stale connections.
        """
        # Note that we do not close the connection here -- somebody
        # may still be reading from it.
        while len(self.queue) > 0 and self._pair_stale(self.queue[0]):
            conn, _ = self.queue.pop(0)
            conn.close()

    def _pair_stale(self, pair):
        """
        Returns true of the (connection,time) pair is too old to be
        used.
        """
        (_conn, return_time) = pair
        now = time.time()
        return return_time + ConnectionPool.STALE_DURATION < now
    
    def close(self):
        for conn, _ in self.queue:
            conn.close()

class ConnectionPool(object):

    """
    A connection pool that expires connections after a fixed period of
    time.  This saves time spent waiting for a connection that AWS has
    timed out on the other end.

    This class is thread-safe.
    """

    #
    # The amout of time between calls to clean all pools.
    #
    
    CLEAN_INTERVAL = 10.0

    # How long before a connection becomes "stale" and won't be reused
    # again.  This must be less than SOCKET_READ_TIMEOUT that is defined
    # in the SocialSearch and SocialWriter servers!

    STALE_DURATION = 12.0

    def __init__(self):
        # Mapping from (host,is_secure) to HostConnectionPool.
        # If a pool becomes empty, it is removed.
        self.host_to_pool = {}
        # The last time the pool was cleaned.
        self.last_clean_time = 0.0
        self.mutex = threading.Lock()

    def size(self):
        """
        Returns the number of connections in the pool.
        """
        return sum(pool.size() for pool in self.host_to_pool.values())

    def get_connection(self, host_port):
        """
        Gets a connection from the pool for the named host.  Returns
        None if there is no connection that can be reused. It's the caller's
        responsibility to call close() on the connection when it's no longer
        needed.
        """
        self.clean()
        with self.mutex:
            key = host_port
            if key not in self.host_to_pool:
                return None
            return self.host_to_pool[key].get()

    def put_connection(self, host_port, conn):
        """
        Adds a connection to the pool of connections that can be
        reused for the named host.
        """
        with self.mutex:
            if conn.expireConnection():
                conn.close()
            else:
                key = host_port
                if key not in self.host_to_pool:
                    self.host_to_pool[key] = HostConnectionPool()
                self.host_to_pool[key].put(conn)

    def clean(self):
        """
        Clean up the stale connections in all of the pools, and then
        get rid of empty pools.  Pools clean themselves every time a
        connection is fetched; this cleaning takes care of pools that
        aren't being used any more, so nothing is being gotten from
        them. 
        """
        with self.mutex:
            now = time.time()
            if self.last_clean_time + self.CLEAN_INTERVAL < now:
                to_remove = []
                for (host, pool) in self.host_to_pool.items():
                    pool.clean()
                    if pool.size() == 0:
                        to_remove.append(host)
                for host in to_remove:
                    del self.host_to_pool[host]
                self.last_clean_time = now
    
    def close(self):
        for key in self.host_to_pool:
            pool = self.host_to_pool[key]
            pool.close()
