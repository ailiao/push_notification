import base64
import time
import APNSWrapper

# Dev token
deviceToken = base64.b64decode("dDOEx0WL6Fl1TDWDj/lGXIdvsPJ3+/72OeGQq30enG4=")
wrapper = APNSWrapper.APNSNotificationWrapper('apns-dev-cert.pem', True)

# Production token
# deviceToken = base64.b64decode("DL2vURMBXGz5gybg8C/nPTY837mfAivEEhzwc29xlqM=")
# wrapper = APNSWrapper.APNSNotificationWrapper('apns-prod-cert.pem', False)
                        
# create message
message = APNSWrapper.APNSNotification()
message.token(deviceToken)
message.badge(1)
message.sound()

alert = APNSWrapper.APNSAlert()
alert.body("Here is a push message!")

alert.action_loc_key("View")
                
message.appendProperty(APNSWrapper.APNSProperty("command", "update_messages"))
message.alert(alert)

print "sending first"
wrapper.append(message)
wrapper.notify()

time.sleep(10)

print "sending second"
wrapper.append(message)
wrapper.notify()
