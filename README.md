# README #

Sample code for sending push notifications in Python

### How do I get set up? ###

* You need a push notification certificate in .pem format.  Development or adhoc/production certificate can be used but be sure to set sandbox to True or False depending on whether it's sandbox or not.

* Device token, which comes from iOS app:
```
#!

didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
```
This example uses a token that was base64 encoded.